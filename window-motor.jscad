// title      : windowMotor
// author     : John Cole
// license    : ISC License
// file       : windowMotor.jscad

/* exported main, getParameterDefinitions */

function getParameterDefinitions() {
  var ENABLED = {
    driveGear: true,
    pinionGear: true,
    topPlate: true,
    bottomPlate: true,
    windowbase: true,
    hardware: true,
  };
  return [
    { type: 'group', name: 'Parts' },
    ...Object.keys(ENABLED).map((name) => {
      return {
        name,
        type: 'checkbox',
        checked: ENABLED[name],
      };
    }),
    {
      name: 'resolution',
      type: 'choice',
      values: [0, 1, 2, 3, 4],
      captions: [
        'low (8,24)',
        'normal (12,32)',
        'high (24,64)',
        'very high (48,128)',
        'ultra high (96,256)',
      ],
      default: 0,
      initial: 0,
      caption: 'Resolution:',
    },
    { type: 'group', name: 'View' },
    {
      name: 'center',
      type: 'checkbox',
      checked: false,
    },
    { type: 'group', name: 'Cutaway' },
    {
      name: 'cutawayEnable',
      caption: 'Enable:',
      type: 'checkbox',
      checked: false,
    },
    {
      name: 'cutawayAxis',
      type: 'choice',
      values: ['x', 'y', 'z'],
      initial: 'y',
      caption: 'Axis:',
    },
  ];
}

function main(params) {
  var start = performance.now();
  var resolutions = [
    [6, 16],
    [8, 24],
    [12, 32],
    [24, 64],
    [48, 128],
  ];
  var [defaultResolution3D, defaultResolution2D] = resolutions[
    parseInt(params.resolution)
  ];
  CSG.defaultResolution3D = defaultResolution3D;
  CSG.defaultResolution2D = defaultResolution2D;
  util.init(CSG, {
    debug: '*,-jscad-gears*,jscadUtils*',
  });

  var parts = gearedWindowMotor(!params.center);

  // return parts['assembled']();
  var selectedParts = Object.entries(parts)
    .filter(([key, value]) => {
      return params[key];
    })
    .reduce((parts, [key, value]) => {
      var part = value();
      if (Array.isArray(part)) parts = parts.concat(part);
      else parts.push(part);
      return parts;
    }, []);

  console.log('selectedParts', selectedParts);
  var parts = selectedParts.length ? selectedParts : [util.unitAxis(20)];

  if (params.center) parts = [union(parts).Center()];
  if (params.cutawayEnable)
    parts = [util.bisect(union(parts), params.cutawayAxis).parts.positive];

  console.log('timer', performance.now() - start);
  console.log(
    'BOM\n',
    Object.entries(Hardware.BOM)
      .map(([key, value]) => `${key}: ${value}`)
      .join('\n')
  );
  return [...parts];
}

function directMotorMount() {
  var windowbase = WindowBase()
    .rotate('base', 'x', 90)
    .rotate('base', 'y', 90)
    .center('base')
    .zero('base');

  console.warn('windowbase * ', windowbase);
  var motor = Motor12v();

  var plate = Parts.RoundedCube(30, 50, 3, 5)
    .align(motor.parts.shaft, 'xy')
    .snap(motor.parts.gearbox, 'z', 'outside+')
    .subtract([motor.combine('pto,screw1,screw2')]);

  motor.add(plate, 'plate');

  var coupler = Parts.Cylinder(20, 20)
    .align(motor.parts.shaft, 'xy')
    .snap(motor.parts.pto, 'z', 'outside+')
    .color('white');

  var centerGear = CenterGear(10, { driveBearingThickness: 0 })
    .align(coupler, 'xy')
    .snap(coupler, 'z', 'inside-')
    .color('red');

  motor.add(
    coupler.subtract([
      centerGear,
      motor
        .combine('pin-clearance,shaft')
        .enlarge([
          Hardware.Clearances.loose,
          Hardware.Clearances.loose,
          Hardware.Clearances.loose,
        ]),
    ]),
    'coupler'
  );
  // coupler.connect(
  //   "cylinder.start",
  //   windowbase.parts.base.properties.driveConnector
  // );
  console.warn('motor', motor);

  if (false) {
    motor.connectTo(
      'coupler',
      'cylinder.start',
      windowbase.parts.base,
      'driveConnector',
      false
    );

    return [windowbase.combine(), motor.combine()];
  } else {
    return motor.combine('coupler');
  }
}

function gearedWindowMotor(connectParts) {
  var shaftlength = 21;
  var motorgap = shaftlength - (params.thickness || 0);

  var motor = Motor(shaftlength, motorgap, params /* , maingear, piniongear */);
  var bearing = Hardware.Bearing(RSeriesBearings['R8']);
  var pinionBearing = Hardware.Bearing({ OD: 22, ID: 8.1, thickness: 7 });

  var gears = WindowGears(8, {
    driveBearingID: bearing.properties.ID - Hardware.Clearances.close,
    driveBearingThickness: bearing.properties.thickness,
    pinionBearingID: pinionBearing.properties.ID - Hardware.Clearances.close,
    pinionBearingThickness: pinionBearing.properties.thickness,
  });

  gears.add(
    bearing.connectTo('bearing', 'start', gears.parts.drive, 'end', false),
    'bottomBearing'
  );

  gears.add(
    bearing
      .clone()
      .connectTo('bearing', 'end', gears.parts.drive, 'start', true),
    'topBearing',
    false,
    'topBearing_'
  );

  gears.add(
    pinionBearing.connectTo(
      'bearing',
      'start',
      gears.parts.pinion,
      'start',
      true
    ),
    'pinionBearing',
    false,
    'pinionBearing_'
  );

  console.log('**** gears', gears);
  var windowbase = WindowBase()
    .rotate('base', 'x', 90)
    .rotate('base', 'y', 90)
    .center('base')
    .zero('base');

  var gearsBBox = gears.combine('drive,pinion', (p) => parts.BBox(p));
  var gearSize = util.size(union(gearsBBox));

  motor.connectTo(
    'shaft',
    'driveConnector',
    gears.parts.pinion,
    'start',
    false
  );

  function connect() {
    motor.connectTo(
      'shaft',
      'driveConnector',
      gears.parts.pinion,
      'start',
      false
    );
    // var bearing2 = bearing.clone();

    // bearing2.connectTo("bearing", "end", gears.parts.drive, "start", true);
  }

  function TopPlate() {
    var topPlate = Parts.RoundedCube(
      gearSize.x + 10,
      bearing.properties.OD + 10,
      pinionBearing.properties.thickness + 2,
      5
    ).align(union(gearsBBox), 'xy');

    return topPlate
      .snap(gears.parts.pinionBearing, 'z', 'inside+')
      .subtract([
        gears.parts.pinionBearing_shell.enlarge([
          Hardware.Clearances.close,
          Hardware.Clearances.close,
          0,
        ]),
        gears.parts.pinionBearing_shell.enlarge([-2, -2, 4]),
        gears.parts.topBearing_shell.enlarge([
          Hardware.Clearances.close,
          Hardware.Clearances.close,
          0,
        ]),
        gears.parts.topBearing_shell.enlarge([-2, -2, 4]),
        ...cornerHoles(topPlate, gears.parts.topBearing),
      ]);
  }

  gears.add(TopPlate(), 'topPlate');

  function cornerHoles(alignXYto, alignZto) {
    var hole = Parts.Cylinder(5, 20)
      .snap(alignXYto, 'xy', 'inside+', -2)
      .align(alignZto, 'z');
    return [
      hole,
      hole.snap(alignXYto, 'xy', 'inside-', 2),

      hole.snap(alignXYto, 'x', 'inside-', 2),

      hole.snap(alignXYto, 'y', 'inside-', 2),
    ];
  }

  function BottomPlate() {
    var bottomPlate = Parts.RoundedCube(
      gearSize.x + 10,
      bearing.properties.OD + 10,
      bearing.properties.thickness + 2,
      5
    ).align(union(gearsBBox), 'xy');

    bottomPlate = bottomPlate.snap(bearing.parts.bearing, 'z', 'inside-');

    var plateToMotorGap = util.calcSnap(
      bottomPlate,
      motor.parts.body,
      'z',
      'outside+'
    );
    var motorSize = util.size(motor.parts.body);

    var motorMount = Parts.Cylinder(motorSize.x, plateToMotorGap[2])
      .snap(bottomPlate, 'z', 'outside-')
      .align(motor.parts.body, 'xy');

    return union([
      bottomPlate,
      motorMount.fillet(-1.5, 'z-').color('orange'),
    ]).subtract([
      bearing.parts.shell.enlarge([
        Hardware.Clearances.close,
        Hardware.Clearances.close,
        0,
      ]),
      bearing.parts.shell.enlarge([-2, -2, 4]),
      Parts.Cylinder(20, 10).align(gears.parts.bottomBearing, 'xyz'),
      motor.combine(
        'screw1_headClearSpace,screw1_tap,screw2_headClearSpace,screw2_tap,collar'
      ),
      motor.parts.shaft.enlarge([
        Hardware.Clearances.loose,
        Hardware.Clearances.loose,
        0,
      ]),
      ...cornerHoles(bottomPlate, gears.parts.bottomBearing),
    ]);
  }

  // var bracket = Bracket(windowbase, 25, 50, 5);

  gears.add(BottomPlate(), 'bottomPlate');
  // var a = util.unitAxis(25, 0.25);
  // a.properties.cp = new CSG.Connector([0, 0, 0], [0, 0, 1], [0, 1, 0]);
  // console.log("Clearances", Hardware.Clearances);

  if (connectParts) {
    var g = gears.connectTo(
      'drive',
      'start',
      windowbase.parts.base,
      'driveConnector',
      true
    );

    connect();
  }
  var parts = {
    center: () => {
      return Parts.Cylinder(20, 5).subtract(gears.parts.center);
      // .union(
      //   Parts.Cylinder(9, 2)
      //     .subtract(Parts.Cylinder(7.4, 2))
      //     .color("gray", 0.75)
      // );
    },
    driveGear: () => {
      return gears.parts.drive;
    },
    pinionGear: () => {
      return gears.parts.pinion.subtract(
        motor.parts.shaft.enlarge([
          Hardware.Clearances.close,
          Hardware.Clearances.close,
          0,
        ])
      );
    },
    topPlate: () => {
      return gears.combine('topPlate');
    },
    bottomPlate: () => {
      return gears.combine('bottomPlate');
    },
    windowbase: () => {
      return windowbase.combine();
    },
    hardware: () => {
      return [
        motor.combine(),
        gears.combine('bottomBearing,topBearing,pinionBearing'),
      ];
    },
    assembled: () => {
      var g = gears.connectTo(
        'drive',
        'start',
        windowbase.parts.base,
        'driveConnector',
        true
      );

      connect();
      // console.log('gears', gears);

      return [
        parts.driveGear(),
        parts.pinionGear(),
        parts.topPlate(),
        parts.bottomPlate(),
        windowbase.combine(),
        motor.combine(),
        gears.combine('bottomBearing,topBearing,pinionBearing'),
      ];
    },
  };
  return parts;
}
// ********************************************************
// Other jscad libraries are injected here.  Do not remove.
// Install jscad libraries using NPM
// ********************************************************
// include:js
// endinject
