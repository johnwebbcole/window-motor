function PlateSize(maingear, piniongear) {
  return {
    x: piniongear.gearInfo.outerRadius + maingear.gearInfo.outerRadius,
    y: piniongear.gearInfo.outerRadius + 20,
  };
}

function BracketPlate(y, x) {
  x = x || 5;
  return CAG.roundedRectangle({
    center: [0, 0],
    radius: [x, y],
    roundradius: 5,
    resolution: 16,
  }).extrude({
    offset: [0, 0, 5],
  });
}

function Bracket(wb, width, height, thickness = 5) {
  var deltaz = -(thickness / 2) - 5 - 5;
  var plate = BracketPlate(height).translate([width + 5, 0, deltaz]);
  // .subtract(bolts.translate([0, 0, -10]));

  var r = 26 / 2;
  var connector = CSG.cube({
    center: [0, 0, 0],
    radius: [r, 5, r],
  });

  var bracket = plate.union(
    CSG.cube({
      center: [0, 0, 0],
      radius: [10, 5, 10],
    })
      .rotateZ(90)
      .translate([width + 5, 0, deltaz - 10])
  );
  return connector
    .translate([-r, width + 5, r])
    .translate([wb.c.x, 0, -wb.c.y]);
  //     .rotateY(-wb.tri.B)
  //     .rotateZ(90)
  //     .rotateX(-90)
  //     .rotateZ(180)
  //     .translate([0, 0, -(thickness / 2) - 5]);
  //   return bracket;
  // .union(
  //   connector
  //     .translate([-r, width + 5, r])
  //     .translate([wb.c.x, 0, -wb.c.y])
  //     .rotateY(-wb.tri.B)
  //     .rotateZ(90)
  //     .rotateX(-90)
  //     .rotateZ(180)
  //     .translate([0, 0, -(thickness / 2) - 5])
  // )
  // .subtract(
  //   wb
  //     .combine()
  //     .translate([wb.c.x, 0, -wb.c.y])
  //     .rotateY(-wb.tri.B)
  //     .rotateZ(90)
  //     .rotateX(-90)
  //     .rotateZ(180)
  //     .translate([0, 0, -(thickness / 2) - 5])
  // );
  // .union(BottomPlate(params, maingear, piniongear));
}
