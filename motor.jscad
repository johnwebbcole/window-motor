function Motor(shaftlength = 21, motorgap, { options }) {
  var g = util.group("motor");

  var body = Parts.Cylinder(25, 45).color("gray");
  g.add(body, "body");

  var collar = Parts.Cylinder(7, 2.5)
    .snap(body, "z", "outside+")
    .color("yellow");
  g.add(collar, "collar");

  var shaft = Parts.Cylinder(5, shaftlength).color("silver");

  shaft.properties.driveConnector = new CSG.Connector(
    [0, 0, 0],
    [0, 0, 1],
    [0, 1, 0]
  );

  var screw = Hardware.Screw.PanHead(
    MetricFlatHeadScrews["M4"],
    11.5,
    "loose",
    {
      name: "screw1",
      clearLength: 10,
    }
  )
    .snap("thread", body, "z", "outside+", 5)
    .snap("thread", body, "y", "inside+", -2);

  g.add(screw, "screw1", false, "screw1_");

  g.add(
    screw.clone((p) => p.mirroredY()),
    "screw2",
    false,
    "screw2_"
  );

  shaft = shaft.snap(collar, "z", "outside+");

  var key = Parts.Cube([5, 5, 8])
    .align(shaft, "xyz")
    .snap(shaft, "z", "inside-")
    .snap(shaft, "x", "outside+")
    .translate([5 - 4.15, 0, 0])
    .color("red");

  g.add(shaft.subtract(key), "shaft");

  // console.log("motor", g.parts);
  return g;
}

function Motor12v() {
  var g = util.group("motor");

  var shaft = Parts.Cylinder(4, 10)
    .addConnector("shaft", [0, 0, 0], [0, 0, 1], [0, 1, 0])
    .translate([4.5, 0, 0])
    .color("silver");

  g.add(shaft, "shaft");

  var pin = Parts.Cylinder(1.6, 7)
    .rotateX(90)
    .align(g.parts.shaft, "xy")
    .snap(g.parts.shaft, "z", "inside-", 2)
    .color("orange");

  g.add(pin, "pin");
  g.add(pin.stretch("z", 8).color("red"), "pin-clearance", true);

  var pto = Parts.Cylinder(8, 3)
    .align(g.parts.shaft, "xy")
    .snap(shaft, "z", "outside-")
    .color("darkgray");
  g.add(pto, "pto");

  var gearbox = Parts.Cylinder(27, 15)
    .snap(pto, "z", "outside-")
    .color("darkgray");

  g.add(gearbox, "gearbox");

  var body = Parts.Cylinder(24, 27)
    .snap(gearbox, "z", "outside-")
    .color("gray");
  g.add(body, "body");
  console.log("shaft", g.parts.shaft);
  console.log("util.unitAxis", util.unitAxis(5, 0.1));
  // g.add(
  //   util.unitAxis(5, 0.1).connect("origin", g.parts.shaft.properties.shaft)
  // );

  var screw = Hardware.Screw.FlatHead(
    MetricFlatHeadScrews["M2.5"],
    6,
    "loose",
    {
      name: "screw1",
      clearLength: 10,
    }
  )
    .snap("thread", gearbox, "z", "outside+", 4.5)
    .snap("thread", gearbox, "y", "inside+", -1);

  g.add(screw, "screw1", false, "screw1_");

  g.add(
    screw.clone((p) => p.mirroredY()),
    "screw2",
    false,
    "screw2_"
  );

  // shaft = shaft.snap(collar, "z", "outside+");

  // var key = Parts.Cube([5, 5, 8])
  //   .align(shaft, "xyz")
  //   .snap(shaft, "z", "inside-")
  //   .snap(shaft, "x", "outside+")
  //   .translate([5 - 4.15, 0, 0])
  //   .color("red");

  // g.add(shaft.subtract(key), "shaft");

  // console.log("motor", g.parts);
  return g;
}
