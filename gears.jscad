function Pinslot(thickness) {
  var ht = thickness / 2;
  return CSG.cube({
    center: [0, 0, 0],
    radius: [0.6, 7, 1.25],
  }).union(
    CSG.cube({
      center: [0, 0, 0],
      radius: [1.25, 1, 1.25],
    }).translate([0, 7, 0])
  );
  // .translate([0, 0, -(ht - 1.25)])
  // .setColor(1, 1, 0);
}

function CenterGear(thickness, options) {
  var qualitySettings = {
    resolution: CSG.defaultResolution2D,
    stepsPerToothAngle: 3,
  };
  var centerGear = new Gear({
    toothCount: 12,
    circularPitch: 2.45,
    pressureAngle: 35,
    qualitySettings,
  });
  var centerShape = centerGear.getZeroedShape();

  return util.poly2solid(
    centerShape,
    centerShape,
    thickness + options.driveBearingThickness * 2
  );
}

function WindowGears(thickness = 5, options) {
  console.log('WindowGears', { thickness, options });
  var qualitySettings = {
    resolution: CSG.defaultResolution2D,
    stepsPerToothAngle: 3,
  };
  // options.holes = true;
  var toothCount = 36;

  var mainPitchDiameter = 50;
  var circularPitch = (Math.PI * mainPitchDiameter) / toothCount;

  var pinionGear = new Gear({
    circularPitch,
    toothCount: 12,
    qualitySettings,
    clearance: 0.5,
    backlash: 0.1,
    // profileShift: 0.5,
  });

  var driveGear = new Gear({
    circularPitch,
    toothCount,
    connectedGear: pinionGear,
    qualitySettings,
    clearance: 0.5,
    backlash: 0.1,
    // profileShift: 0.5,
  });

  var gearset = new GearSet(driveGear, pinionGear);

  var drivePost = Parts.Cylinder(
    options.driveBearingID,
    options.driveBearingThickness
  ).translate([0, 0, thickness - 0.01]);

  var driveShape = gearset.createShape(1);
  var drive = util.poly2solid(driveShape, driveShape, thickness);
  drive.properties.start = new CSG.Connector(
    [0, 0, -thickness],
    [0, 0, 1],
    [1, 0, 0]
  );

  drive.properties.end = new CSG.Connector(
    [0, 0, thickness],
    [0, 0, 1],
    [1, 0, 0]
  );

  var driveSize = util.size(drive);

  var pinionShape = gearset.createShape(2);
  var pinion = util
    .poly2solid(pinionShape, pinionShape, thickness)
    .color('pink');
  var pinionCentroid = util.centroid(pinion);

  pinion.properties.start = new CSG.Connector(
    [pinionCentroid.x, pinionCentroid.y, 0],
    [0, 0, 1],
    [1, 0, 0]
  );

  // var centerGear = new Gear({
  //   toothCount: 12,
  //   circularPitch: 2.45,
  //   pressureAngle: 35,
  //   qualitySettings,
  // });
  // var centerShape = centerGear.getZeroedShape();

  // var center = util
  //   .poly2solid(
  //     centerShape,
  //     centerShape,
  //     thickness + options.driveBearingThickness * 2
  //   )
  //   .align(drive, "xyz")
  //   .color("red");

  var center = CenterGear(thickness, options)
    .align(drive, 'xyz')
    .color('red');

  var pinionPost = Parts.Cylinder(
    options.pinionBearingID,
    options.pinionBearingThickness
  )
    .fillet(-2, 'z+')
    .align(pinion, 'xy')
    .snap(pinion, 'z', 'outside+', 0.01)
    .color('pink');

  var pinslot = Parts.Cube([2.0 + options.pinionBearingThickness, 14, 2.5])
    .union(Parts.Cube([3 + options.pinionBearingThickness, 2, 2.5]))
    .rotateY(-90)
    .align(pinion, 'xy')
    .snap(pinionPost, 'z', 'inside-')
    .color('red');

  if (options.holes) {
    var driveHole = Parts.Cylinder(8, thickness)
      .fillet(-2, 'z+')
      .fillet(-2, 'z-')
      .align(drive, 'xy')
      .translate([driveSize.x / 3 - 2, 0, 0])
      .color('red');

    var holeAngles = [0, 60, 120, 180, 240, 300];

    var driveHoles = holeAngles.map((a) => driveHole.rotateZ(a));

    var pinionHole = Parts.Cylinder(3, thickness)
      .fillet(-1, 'z+')
      .fillet(-1, 'z-')
      // .align(pinion, "xy")
      .translate([8.5, 0, 0])
      .color('red');

    var pinionHoles = union(holeAngles.map((a) => pinionHole.rotateZ(a))).align(
      pinion,
      'xy'
    );
  } else {
    var driveHoles = [];
    var pinionHoles = [];
  }

  return util.group({
    drive: drive
      // .chamfer(0.5, "z-")
      // .chamfer(0.5, "z+")
      .union(drivePost.fillet(-2, 'z-'))
      .union(drivePost.snap(drive, 'z', 'outside+').fillet(-2, 'z+'))
      .color('skyblue')
      .subtract([center, ...driveHoles]),
    pinion: pinion
      .union(pinionPost)
      .subtract([pinslot, pinionHoles])
      .chamfer(0.5, 'z+'),
    center,
  });
}
