util = {
  print: function (msg, o) {
    echo(msg, JSON.stringify(o.getBounds()), JSON.stringify(this.size(o.getBounds())));
  },

  label: function label(text, x, y, width, height) {
    var l = vector_text(x || 0, y || 0, text); // l contains a list of polylines to draw
    var o = [];
    l.forEach(function (pl) { // pl = polyline (not closed)
      o.push(rectangular_extrude(pl, {
        w: width || 2,
        h: height || 2
      })); // extrude it to 3D
    });
    return this.center(union(o));
  },

  divxyz: function (size, x, y, z) {
    return {
      x: size.x / x,
      y: size.y / y,
      z: size.z / z
    };
  },

  div: function (size, d) {
    return this.divxyz(size, d, d, d);
  },

  mulxyz: function (size, x, y, z) {
    return {
      x: size.x * x,
      y: size.y * y,
      z: size.z * z
    };
  },

  mul: function (size, d) {
    return this.divxyz(size, d, d, d);
  },

  xyz2array: function xyz2array(size) {
    return [size.x, size.y, size.z];
  },

  size: function size(bbox) {
    return {
      x: bbox[1].x - bbox[0].x,
      y: bbox[1].y - bbox[0].y,
      z: bbox[1].z - bbox[0].z
    };
  },

  center: function center(object, size) {
    size = size || this.size(object.getBounds());
    return this.centerY(this.centerX(object, size), size);
  },

  centerY: function centerY(object, size) {
    size = size || this.size(object.getBounds());
    return object.translate([0, -size.y / 2, 0]);
  },

  centerX: function centerX(object, size) {
    size = size || this.size(object.getBounds());
    return object.translate([-size.x / 2, 0, 0]);
  },

  enlarge: function enlarge(object, x, y, z) {
    var size = this.size(object.getBounds());

    function scale(size, value) {
      return 1 + ((size / 100) * value);
    }
    var s = [scale(size.x, x), scale(size.y, y), scale(size.z, z)];
    return object.scale(s);
  },

  shift: function shift(object, x, y, z) {
    var hsize = this.div(this.size(object.getBounds()), 2);
    echo('hsize', JSON.stringify(hsize));
    return object.translate(this.xyz2array(this.mulxyz(hsize, x, y, z)));
  },

  zero: function shift(object) {
    var bounds = object.getBounds();
    echo('zero', JSON.stringify(bounds));
    return object.translate([0, 0, -bounds[0].z]);
  },

  mirrored4: function mirrored4(x) {
    return x.union([x.mirroredY(90), x.mirroredX(90), x.mirroredY(90).mirroredX(90)]);
  },

  /**
   * Moves an object flush with another object
   * @param  {CSG} moveobj Object to move
   * @param  {CSG} withobj Object to make flush with
   * @param  {String} axis    Which axis: 'x', 'y', 'z'
   * @param  {Number} mside   0 or 1
   * @param  {Number} wside   0 or 1
   * @return {CSG}         [description]
   */
  flush: function flush(moveobj, withobj, axis, mside, wside) {
    // mside: 0 or 1
    // axis: 'x', 'y', 'z'
    wside = wside !== undefined ? wside : mside;
    var m = moveobj.getBounds();
    var w = withobj.getBounds()
    var d = w[wside][axis] - m[mside][axis];
    var ta = [d, 0, 0];
    if (axis != 'x') {
      ta = axis == 'y' ? [0, d, 0] : [0, 0, d];
    }
    // echo('flush', JSON.stringify(ta));
    return moveobj.translate(ta)
  },

  init: function init(CSG) {
    /**
     * Moves an object flush with another object
     * @param  {CSG} obj   withobj Object to make flush with
     * @param  {String} axis    Which axis: 'x', 'y', 'z'
     * @param  {Number} mside   0 or 1
     * @param  {Number} wside   0 or 1
     * @return {CSG}       [description]
     */
    CSG.prototype.flush = function flush(obj, axis, mside, wside) {
      return util.flush(this, obj, axis, mside, wside);
    }
  }

};