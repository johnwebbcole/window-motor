include('Utils.jscad');
include('lodash.js');


_boardutils = {
  CornerHole: function makeCornerHole(r, size, center) {
    center = center || r;
    return CSG.cylinder({
      start: [0, 0, 0],
      end: [0, 0, size.z * 2],
      radius: r,
      resolution: 64
    }).translate([-(size.x / 2) + (center), (size.y / 2) - (center), 0]).setColor(0.75, 0, 0);
  },


  Hole: function makeHole(r, h, x) {
    return CSG.cylinder({
      start: [0, 0, -h],
      end: [0, 0, h],
      radius: r,
      resolution: 64
    }).translate([(x / 2) - (r + 2.5), 0, h]).setColor(0.75, 0.75, 0);
  },

  Corners: function Corners(board, z) {
    var boardsize = util.size(board.getBounds());

    var r = {
      height: 5,
      width: 4
    };

    var c = {
      x: 2.5,
      y: 2.5,
      z: z || 2
    };

    var inset = 1.5;
    var corner = CSG.cube({
        center: [0, 0, c.z],
        radius: [c.x, c.y, c.z]
      })
      .translate([(boardsize.x / 2) - inset, (boardsize.y / 2) - inset, 0])
      .subtract(board.translate([0, 0, r.height - 2.25]))
      .subtract(board.scale([0.92, 0.95, 2]).translate([0, 0, 1]))
      .setColor(0, 0, 1);

    var corners = util.mirrored4(corner);
    return corners;
  },

  CenterHoles: function (hole_r, boardinfo) {
    hole = _boardutils.Hole(hole_r, boardinfo.size.z, boardinfo.size.x);
    return union([hole, hole.mirroredX(90)]);
  },

  CornerHoles: function (hole_r, boardinfo) {
    return util.mirrored4(_boardutils.CornerHole(hole_r, boardinfo.size, 3.5));
  }
}

BoardGenerator = {
  UnitCube: function () {
    return CSG.cube({
      center: [0, 0, 0],
      radius: [1, 1, 1]
    });
  },

  Boards: {
    ProtoBoardHalf: {
      roundradius: 2.5,
      size: {
        x: 81.5,
        y: 51,
        z: 1.6
      },
      holes: _boardutils.CenterHoles
    },
    ProtoBoardQuarter: {
      roundradius: 2.5,
      size: {
        x: 43.25,
        y: 51,
        z: 1.6
      },
      holes: _boardutils.Hole
    },
    ProtoBoardSmallTin: {
      roundradius: 6,
      size: {
        x: 53.4,
        y: 33.1,
        z: 1.6
      },
      holes: _boardutils.CornerHoles
    },
    BreadBoardHalf: {
      roundradius: 0,
      size: {
        x: 82.35,
        y: 54.6,
        z: 9.5
      },
      holes: _boardutils.CenterHoles
    }
  },

  ProtoBoard: function (board) {
    hole_r = 2.8 / 2;

    var protoboard = CAG.roundedRectangle({
      center: [0, 0, 0],
      radius: util.div(board.size, 2),
      roundradius: board.roundradius,
      resolution: 360
    }).extrude({
      offset: [0, 0, board.size.z]
    }).setColor(0.5, 0.5, 0.5, 0.25);

    // hole = _boardutils.Hole(hole_r, board.size.z, board.size.x);
    //
    // holes = union([hole, hole.mirroredX(90)]);
    echo(board.holes);
    holes = board.holes(hole_r, board);
    //return protoboard.subtract(holes);

    var riser = CAG.fromPoints([
        [-1.425, 0],
        [-1.58, 1.63],
        [1.58, 1.63],
        [1.425, 0]
      ], true).extrude({
        offset: [0, 0, 4.75]
      })
      //.rotateX(90)
      //.translate([-(12.5 + r.width - 2), (19.7 - 6), 0])
      //.subtract(board.translate([0, 0, r.height - 2.25]).scale([1, 1, 1.025]))
      .setColor(0, 0, 1);

    return protoboard.union(riser.translate([0, board.size.y / 2, 0]));
  },

  Cradle: function (boardinfo) {
    var board = this.ProtoBoard(boardinfo);
    return _boardutils.Corners(board);
  },

  Create: function (options) {
    // return this.UnitCube();
    return this['ProtoBoard'](this.Boards.BreadBoardHalf);
  }
};