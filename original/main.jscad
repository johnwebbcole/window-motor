// title      : OpenJSCAD.org Logo
// author     : Rene K. Mueller
// license    : MIT License
// revision   : 0.003
// tags       : Logo,Intersection,Sphere,Cube
// file       : logo.jscad


include("GearGenerator.jscad");
include("BoardGenerator.jscad");
include('Utils.jscad');

function getParameterDefinitions() {
  return [{
    name: 'type',
    type: 'choice',
    values: ['involuteGear', 'doubleHelical', 'placeHolder'],
    captions: ['Involute', 'Double-helical', 'Place Holder'],
    initial: 'placeHolder',
    caption: "Gear Type:"
  }, {
    name: 'numTeeth',
    caption: 'Main Gear Teeth:',
    type: 'int',
    initial: 48
  }, {
    name: 'pinionTeeth',
    caption: 'Pinion Gear Teeth:',
    type: 'int',
    initial: 16
  }, {
    name: 'circularPitch',
    caption: 'Circular pitch:',
    type: 'float',
    initial: 4
  }, {
    name: 'pressureAngle',
    caption: 'Pressure angle:',
    type: 'float',
    initial: 22
  }, {
    name: 'twist',
    caption: 'Twist:',
    type: 'float',
    initial: 0
  }, {
    name: 'clearance',
    caption: 'Clearance:',
    type: 'float',
    initial: 1
  }, {
    name: 'thickness',
    caption: 'Thickness:',
    type: 'float',
    initial: 15
  }, {
    name: 'show',
    type: 'choice',
    values: ['assembled', 'exploded', 'maingear', 'piniongear', 'topplate', 'protoboard', 'bottomplate', 'bracket', 'topbracket', 'windowbase'],
    captions: ['Assembled', 'Exploded', 'Main Gear', 'Pinion Gear', 'Top Plate', 'Prototype Board', 'Bottom Plate', 'Bracket', 'Top Bracket', 'Window Base'],
    initial: 'topbracket',
    caption: "Show:"
  }];
}

function Bolt(height) {
  return CSG.cylinder({
    start: [0, 0, 0],
    end: [0, height, 0],
    radius: 3,
    resolution: 16
  }).rotateX(90);
}

function Center(height) {
  centerOptions = {
    type: "involuteGear",
    numTeeth: 12,
    circularPitch: 2.3,
    pressureAngle: 33,
    twist: 0,
    clearance: -0.5,
    thickness: height,
    centerholeradius: 2,
    // outerRadius: 4.75,
    // rootRadius: 3.9
  };

  return GearGenerator.Create(centerOptions);
}

function Bearing(height) {
  return CSG.cylinder({
    start: [0, 0, 0],
    end: [0, height, 0],
    radius: 7.5,
    resolution: 128
  }).rotateX(90);
}

function BottomBearing(height, clearance) {
  var bottomEdge = height / 2;
  clearance = clearance || 0;

  var getCyl = function (radius, clearance) {
    return CSG.cylinder({
      start: [0, -bottomEdge, 0],
      end: [0, -(bottomEdge - 2), 0],
      radius: radius + clearance,
      resolution: 256
    });
  };

  return getCyl(10, clearance)
    .subtract(getCyl(8, -clearance))
    .rotateX(90);
}

function Dhole(thickness, dheight) {
  var ht = thickness / 2;
  echo('ht', ht);
  var dhole = CSG.cylinder({
    start: [0, -ht, 0],
    end: [0, ht, 0],
    radius: 2.5,
    resolution: 64
  }).rotateX(90);

  var hdh = dheight / 2;
  var key = CSG.cube({
    center: [0, 0, 0],
    radius: [2.5, 2.5, hdh]
  }).translate([0, (2.5 + (0.79 * 2)), hdh - ht]).setColor(1, 1, 0);

  return dhole.subtract(key);
}

function Pinslot(thickness) {
  var ht = thickness / 2;
  return CSG.cube({
    center: [0, 0, 0],
    radius: [0.6, 7, 1.25]
  }).union(CSG.cube({
    center: [0, 0, 0],
    radius: [1.25, 1, 1.25]
  }).translate([0, 7, 0])).translate([0, 0, -(ht - 1.25)]).setColor(1, 1, 0);
}

function Motor(shaftlength, motorgap, params, mg, pg) {
  var shaft = CSG.cylinder({
    start: [0, 0, 0],
    end: [0, 0, -(shaftlength)],
    radius: 7.5 / 2,
    resolution: 16
  });

  var screw = CSG.cylinder({
    start: [0, 0, 0],
    end: [0, 0, -motorgap],
    radius: 3.2 / 2,
    resolution: 16
  }).translate([0, (25 / 2) - 4, 0]);

  // screw length 11mm, 4mm screw hole
  var depth = (11 - 4) - motorgap;
  var screwhead = CSG.cylinder({
    start: [0, 0, 0],
    end: [0, 0, depth],
    radius: 6 / 2,
    resolution: 16
  }).translate([0, (25 / 2) - 4, -motorgap - depth]);

  var motor = CSG.cylinder({
      start: [0, 0, 0],
      end: [0, 0, 27],
      radius: 25 / 2,
      resolution: 16
    }).union(shaft)
    .union(screw.union(screwhead))
    .union(screw.union(screwhead).mirroredY())
    .translate([mg.gearInfo.pitchRadius + pg.gearInfo.pitchRadius, 0, motorgap + (params.thickness / 2)]).setColor(1, 0, 1);

  return motor;
}

function toRadians(deg) {
  return deg / 180 * Math.PI;
}

function toDegrees(rad) {
  return rad * (180 / Math.PI);
}

function triSolve(p1, p2) {
  var r = {
    A: 90,
    b: Math.abs(p1.y - p2.y),
    c: Math.abs(p2.x - p1.x)
  }

  r.a = Math.sqrt(Math.pow(r.b, 2) + Math.pow(r.c, 2) - 2 * r.b * r.c * Math.cos(toRadians(r.A)));
  r.B = toDegrees(Math.asin(Math.sin(toRadians(r.A)) / r.a * r.c));
  r.C = 90 - r.B;

  return r;
}

function centerHyp(p1, p2) {
  var tri = triSolve(p1, p2);
  var a = tri.a / 2;
  var b = a * Math.sin(toRadians(tri.B));
  var c = a * Math.sin(toRadians(tri.C));
  return {
    a: a,
    b: b,
    c: c,
    x: b + Math.abs(p1.x),
    y: c + p2.y
  };
}

function WindowBase() {
  endWidth = 60;
  middleWidth = 15;
  centerWidth = 12;
  endpiece = function (height) {
      return CSG.Polygon.createFromPoints([
        [0, 0, height],
        [0, 21, height],
        [-18, 21, height],
        [-21, 17, height],
        [-21, 10, height],
        [-20, 0, height]
      ]);
    },
    centerpiece = function (height) {
      return CSG.Polygon.createFromPoints([
        [0, 0, height],
        [0, 26, height],
        [-23, 39.5, height],
        [-38, 23.5, height],
        [-21, 0, height]
      ]);
    }

  var end = endpiece(endWidth).solidFromSlices({
    numslices: 2,
    callback: function (t) {
      return t ? endpiece(middleWidth) : endpiece(endWidth);
    }
  });

  var thing = endpiece(middleWidth).solidFromSlices({
    numslices: 2,
    callback: function (t) {
      return t ? centerpiece(centerWidth) : endpiece(middleWidth);
    }
  });

  var center = centerpiece(centerWidth).solidFromSlices({
    numslices: 2,
    callback: function (t) {
      return t ? centerpiece(0) : centerpiece(centerWidth);
    }
  });

  var hole = CSG.cylinder({
    start: [0, 0, 0],
    end: [0, 40, 0],
    radius: 1.5,
    resolution: 32
  }).translate([0 - 5, 0, endWidth - 10]);

  var left = end.union([thing, center, hole])
  return {
    tri: triSolve({
      x: -23,
      y: 39.5
    }, {
      x: -38,
      y: 23.5
    }),
    c: centerHyp({
      x: -23,
      y: 39.5
    }, {
      x: -38,
      y: 23.5
    }),
    o: left.union(left.mirroredZ()).rotateX(90).setColor(160 / 255, 82 / 255, 45 / 255, 0.5)
  };
}

function MainGear(params, motorgap, shaftlength) {
  var mg = GearGenerator.Create(_.clone(params));
  var bearing = Bearing(motorgap).translate([0, 0, params.thickness / 2]);
  var center = Center(motorgap + params.thickness + 2);

  var b = BottomBearing(params.thickness, 0.25).setColor(0.75, 0, 0);

  return {
    gearInfo: mg.gearInfo,
    bearingSize: util.size(bearing.getBounds()),
    topBearing: bearing,
    centerGear: center,
    gear: mg.gear.setColor(0, 1, 0, 0.75)
      .union(bearing)
      .subtract(center.gear.setColor(0, 0, 1).translate([0, 0, (shaftlength / 2) - (params.thickness / 2)]))
      .subtract(b)
  };
}

function PinionGear(params, maingear) {
  var pinionParams = _.assign(_.clone(params), {
    numTeeth: params.pinionTeeth,
    twist: params.twist * -1
  });
  var pg = GearGenerator.Create(pinionParams);

  var pr = maingear.gearInfo.pitchRadius + pg.gearInfo.pitchRadius;

  return {
    gearInfo: pg.gearInfo,
    totalPitchRadius: pr,
    gear: pg.gear
      .setColor(0, 0, 1, 0.5)
      .subtract(Dhole(params.thickness, 8).scale([1.1, 1.1, 1]))
      .subtract(Pinslot(params.thickness).setColor(0, 1, 0))
      .translate([pr, 0, 0])
  };
}

function PlateSize(maingear, piniongear) {
  return {
    x: piniongear.gearInfo.outerRadius + maingear.gearInfo.outerRadius,
    y: piniongear.gearInfo.outerRadius + 20
  };
}

function Plate(params, maingear, piniongear, pz) {
  var ps = PlateSize(maingear, piniongear);

  var plate = CAG.roundedRectangle({
      center: [0, 0],
      radius: [ps.x, ps.y],
      roundradius: 5,
      resolution: 16
    })
    .extrude({
      offset: [0, 0, pz]
    })

  return plate;
}

function Standoff(height) {
  return CSG.cylinder({
    start: [0, 0, 0],
    end: [0, height, 0],
    radius: 5,
    resolution: 16
  }).rotateX(90).translate([0, 0, -height / 2]).setColor(0, 0.25, 0);
}

function CopyPlateCorners(o, maingear, piniongear) {
  var mor = maingear.gearInfo.outerRadius,
    por = piniongear.gearInfo.outerRadius
  return union([
    o.translate([-mor + 4, mor - 5, 0]),
    o.translate([-mor + 4, -mor + 5, 0]),
    o.translate([mor + por + 5, mor - 5, 0]),
    o.translate([mor + por + 5, -mor + 5, 0])
  ])
}

function BottomPlate(params, maingear, piniongear) {
  var height = 5,
    deltaz = (-params.thickness / 2) - height;

  var plate = Plate(params, maingear, piniongear, height)
    .translate([piniongear.gearInfo.pitchRadius, 0, deltaz])
    .setColor(0, 1, 0, 0.25)
    .subtract(Bearing(5).translate([0, 0, deltaz]).setColor(0, 1, 0))
    .union(BottomBearing(params.thickness, -0.25).setColor(0, 1, 0))
    .subtract(CSG.cylinder({
      start: [0, 0, 0],
      end: [0, height, 0],
      radius: piniongear.gearInfo.baseRadius,
      resolution: 32
    }).rotateX(90).translate([piniongear.totalPitchRadius, 0, deltaz]));

  return plate.union(CopyPlateCorners(Standoff(params.thickness + 1), maingear, piniongear));
}

function TopPlate(params, maingear, piniongear, motorgap) {
  var deltaz = params.thickness / 2;

  var plate = Plate(params, maingear, piniongear, motorgap)
    .translate([piniongear.gearInfo.pitchRadius, 0, deltaz])
    .setColor(1, 0, 0, 0.25)

  // util.print('TopPlate', plate);
  return plate;
}

function BracketPlate(y, x) {
  x = x || 5;
  return CAG.roundedRectangle({
      center: [0, 0],
      radius: [x, y],
      roundradius: 5,
      resolution: 16
    })
    .extrude({
      offset: [0, 0, 5]
    });
}

function TopBracket(params, maingear, piniongear, bolts, topplate) {
  // util.print('tp', topplate);
  var ps = PlateSize(maingear, piniongear);
  var deltaz = (params.thickness / 2) + 4 + 5;
  var plate = BracketPlate(ps.y, 25)
    .flush(topplate, 'z', 0, 1)
    .flush(topplate, 'x', 1, 0)
    .translate([10, 0, 0]);

  var riser = CSG.cube({
      center: [0, 0, 0],
      radius: [15, 5, 25]
    })
    // .rotateZ(90)
    .flush(plate, 'z', 0, 1)
    .flush(plate, 'x', 0)
    .flush(plate, 'y', 0)
    .translate([10, 5, 0]);

  return plate
    .union(riser)
    .subtract(bolts.translate([0, 0, 10]));
}

function Bracket(params, maingear, piniongear, bolts) {
  var wb = WindowBase();

  var ps = PlateSize(maingear, piniongear);
  var deltaz = -(params.thickness / 2) - 5 - 5;
  var plate = BracketPlate(ps.y).translate([ps.x + 5, 0, deltaz]).subtract(bolts.translate([0, 0, -10]));

  var r = 26 / 2;
  var connector = CSG.cube({
    center: [0, 0, 0],
    radius: [r, 5, r]
  });

  var bracket = plate.union(CSG.cube({
    center: [0, 0, 0],
    radius: [10, 5, 10]
  }).rotateZ(90).translate([ps.x + 5, 0, deltaz - 10]));

  return bracket
    .union(connector.translate([-r, ps.x + 5, r]).translate([wb.c.x, 0, -wb.c.y])
      .rotateY(-wb.tri.B)
      .rotateZ(90)
      .rotateX(-90)
      .rotateZ(180)
      .translate([0, 0, -(params.thickness / 2) - 5]))
    .subtract(wb.o
      .translate([wb.c.x, 0, -wb.c.y])
      .rotateY(-wb.tri.B)
      .rotateZ(90)
      .rotateX(-90)
      .rotateZ(180)
      .translate([0, 0, -(params.thickness / 2) - 5]))
    // .union(BottomPlate(params, maingear, piniongear));

}

function main(params) {
  util.init(CSG);
  echo('params', JSON.stringify(params));
  // var motorgap = 15.5;
  var shaftlength = 24;
  var motorgap = shaftlength - params.thickness;
  // var label = util.label('V3', 0, 0, 4, 0.5).scale([0.1, 0.1, 1]).translate([0, 12, 3.5]).setColor(0, 0, 0);

  var maingear = MainGear(params, motorgap, shaftlength);

  var piniongear = PinionGear(params, maingear);

  var plate = TopPlate(params, maingear, piniongear, motorgap);

  var unitCube = CSG.cube({
    center: [0, 0, 0],
    radius: [0.5, 0.5, 100]
  }).setColor(1, 0, 0);

  var unitAxis = unitCube.union([unitCube.rotateY(90).setColor(0, 1, 0), unitCube.rotateX(90).setColor(0, 0, 1)]);

  var motor = Motor(shaftlength, motorgap, params, maingear, piniongear);

  var bolts = CopyPlateCorners(Bolt(60), maingear, piniongear).translate([0, 0, -params.thickness]);


  var show = {
    assembled: function () {
      var assembly = union([maingear.gear, piniongear.gear, motor, this.topplate(true), this.bottomplate(true), this.bracket()])
        // return assembly;
      var wb = this.windowbase();
      var all = assembly
        .union(wb)
        // .translate([0, 0, -47.8 - 5 - (params.thickness / 2)])

      // .rotateY(-windowbase.tri.B);
      // return util.zero(all);
      return all;
    },
    exploded: function () {
      return union(util.zero(util.shift(this.maingear(), 0, 1, 0)), util.zero(util.shift(this.piniongear(), 0.5, 1, 0)), util.zero(util.shift(this.topplate().rotateX(180), 0, -1.1, 0)));
    },
    maingear: function () {
      return maingear.gear;
    },
    piniongear: function () {
      return piniongear.gear.rotateX(180);
    },
    topplate: function (assembled) {
      var tp = plate.subtract(util.enlarge(maingear.topBearing, 0.5, 0.5, 0)).subtract(motor).subtract(bolts)
      if (assembled)
        return tp;
      else
        return util.zero(tp.rotateX(180));
    },
    protoboard: function () {
      return BoardGenerator.Create(params).union(unitCube);
    },

    bottomplate: function (assembled) {
      var bp = BottomPlate(params, maingear, piniongear)
        .subtract(bolts);
      if (assembled)
        return bp;
      else
        return util.zero(bp)
    },

    bracket: function () {
      return Bracket(params, maingear, piniongear, bolts);
    },

    windowbase: function () {
      var wb = WindowBase();

      return wb.o
        .translate([wb.c.x, 0, -wb.c.y])
        .rotateY(-wb.tri.B)
        .rotateZ(90)
        .rotateX(-90)
        .rotateZ(180)
        .translate([0, 0, -(params.thickness / 2) - 5])
    },

    topbracket: function () {
      var tp = this.topplate(true);
      var tb = TopBracket(params, maingear, piniongear, bolts, tp);
      // return tb.union(tp);
      return tb;
    }
  };

  // return [maingear, piniongear, plate];
  // return [unitCube, maingear, piniongear, plate.subtract(util.enlarge(bearing, 0.5, 0.5, 0)).subtract(motor)];
  return show[params.show]();
}