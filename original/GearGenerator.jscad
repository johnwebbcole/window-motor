include('Utils.jscad');
include('lodash.js');

GearGenerator = {
  UnitCube: function () {
    return CSG.cube({
      center: [0, 0, 0],
      radius: [1, 1, 1]
    });
  },

  gearInfo: function (options) {
    var addendum = options.circularPitch / Math.PI;
    var dedendum = addendum + options.clearance;

    // radiuses of the 4 circles:
    var pitchRadius = options.numTeeth * options.circularPitch / (2 * Math.PI);
    var baseRadius = pitchRadius * Math.cos(Math.PI * options.pressureAngle / 180);
    var outerRadius = options.outerRadius || pitchRadius + addendum;
    var rootRadius = options.rootRadius || pitchRadius - dedendum;
    var maxtanlength = Math.sqrt(outerRadius * outerRadius - baseRadius * baseRadius);
    var maxangle = maxtanlength / baseRadius;

    var tl_at_pitchcircle = Math.sqrt(pitchRadius * pitchRadius - baseRadius * baseRadius);
    var angle_at_pitchcircle = tl_at_pitchcircle / baseRadius;
    var diffangle = angle_at_pitchcircle - Math.atan(angle_at_pitchcircle);
    var angularToothWidthAtBase = Math.PI / options.numTeeth + 2 * diffangle;

    var helix_angle = options.twist;
    var ha = 90 - helix_angle;
    var twst = 360 * (options.thickness / tan(ha)) / (2 * Math.PI * pitchRadius);

    // build a single 2d tooth in the 'points' array:
    var resolution = 10;

    return _.assign({
      addendum: addendum,
      dedendum: dedendum,

      // radiuses of the 4 circles:// radiuses of the 4 circles
      pitchRadius: pitchRadius,
      baseRadius: baseRadius,
      outerRadius: outerRadius,
      rootRadius: rootRadius,

      maxtanlength: maxtanlength,
      maxangle: maxangle,

      tl_at_pitchcircle: tl_at_pitchcircle,
      angle_at_pitchcircle: angle_at_pitchcircle,
      diffangle: diffangle,
      angularToothWidthAtBase: angularToothWidthAtBase,

      helix_angle: helix_angle,
      ha: ha,
      twst: twst,

      // build a single 2d tooth in the 'points' array:// build a single 2d tooth in the 'points' array
      resolution: resolution,
    }, options);
  },

  /*
    For gear terminology see:
      http://www.astronomiainumbria.org/advanced_internet_files/meccanica/easyweb.easynet.co.uk/_chrish/geardata.htm
    Algorithm based on:
      http://www.cartertools.com/involute.html

    circularPitch: The distance between adjacent teeth measured at the pitch circle
  */
  createGear: function createGear(gear) {
    gear.angularToothWidthAtBase = gear.angularToothWidthAtBase
    var points = [new CSG.Vector2D(0, 0)];
    for (var i = 0; i <= gear.resolution; i++) {
      // first side of the tooth:
      var angle = gear.maxangle * i / gear.resolution;
      var tanlength = angle * gear.baseRadius;
      var radvector = CSG.Vector2D.fromAngle(angle);
      var tanvector = radvector.normal();
      var p = radvector.times(gear.baseRadius).plus(tanvector.times(tanlength));
      // echo('i:', i, 'p:', p, 'angle:', angle, 'tanlength:', tanlength, 'radvector:', radvector, 'tanvector:', tanvector, 'baseRadius:', gear.baseRadius);
      points[i + 1] = p;

      // opposite side of the tooth:
      radvector = CSG.Vector2D.fromAngle(gear.angularToothWidthAtBase - angle);
      tanvector = radvector.normal().negated();
      p = radvector.times(gear.baseRadius).plus(tanvector.times(tanlength));
      points[2 * gear.resolution + 2 - i] = p;
      // echo('point2', i, p);
    }

    // create the polygon and extrude into 3D:
    var tooth3d = new CSG.Polygon2D(points).extrude({
      offset: [0, 0, gear.thickness],
      twistangle: gear.twst,
      twiststeps: gear.thickness * 2
    });

    // return tooth3d;
    var allteeth = new CSG();
    for (var j = 0; j < gear.numTeeth; j++) {
      var ang = j * 360 / gear.numTeeth;
      var rotatedtooth = tooth3d.rotateZ(ang);
      allteeth = allteeth.unionForNonIntersecting(rotatedtooth);
    }

    // build the root circle:
    points = [];
    var toothAngle = 2 * Math.PI / gear.numTeeth;
    var toothCenterAngle = 0.5 * gear.angularToothWidthAtBase;
    for (var k = 0; k < gear.numTeeth; k++) {
      var angl = toothCenterAngle + k * toothAngle;
      var p1 = CSG.Vector2D.fromAngle(angl).times(gear.rootRadius);
      points.push(p1);
    }

    // create the polygon and extrude into 3D:
    var rootcircle = new CSG.Polygon2D(points).extrude({
      offset: [0, 0, gear.thickness]
    });

    var result = rootcircle.union(allteeth);

    // center at origin:
    result = result.translate([0, 0, -gear.thickness / 2]);

    return result;
  },

  involuteGear: function (options) {
    var gearInfo = this.gearInfo(options);
    var gear = this.createGear(gearInfo);
    return {
      gear: gear,
      gearInfo: gearInfo
    };
  },

  doubleHelical: function (options) {
    options = _.assign(options, {
      thickness: options.thickness / 2
    });

    var gearInfo = this.gearInfo(options);
    var half = this.createGear(gearInfo);

    var gear = union([
      half.translate([0, 0, options.thickness / 2]), half.mirroredZ().translate([0, 0, -options.thickness / 2])
    ]);

    return {
      gear: gear,
      gearInfo: gearInfo
    };
  },

  placeHolder: function (options) {
    var gearInfo = this.gearInfo(options);

    return {
      gear: CSG.cylinder({ // object-oriented
        start: [0, 0, -(options.thickness / 2)],
        end: [0, 0, options.thickness / 2],
        radius: gearInfo.outerRadius, // true cylinder
        resolution: 16
      }),
      gearInfo: gearInfo
    };
  },

  Create: function (options) {
    return this[options.type](options);
  }


};