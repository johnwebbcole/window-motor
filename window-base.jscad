function centerHyp(p1, p2) {
  var tri = util.triangle.solve(p1, p2);
  var a = tri.a / 2;
  var b = a * Math.sin(util.triangle.toRadians(tri.B));
  var c = a * Math.sin(util.triangle.toRadians(tri.C));
  console.log(p1, p2, a, b, c, tri);
  y = p1.y * (p2.x - x) + (p2.y * (x - p1.x)) / (p2.x - p1.x);
  return {
    a: a,
    b: b,
    c: c,
    x: (b + Math.abs(p1.x)) * (p1.x > 0 ? 1 : -1),
    y: c + p2.y,
    check: polygon([
      [p1.x, p1.y],
      [p2.x, p2.y],
      [p1.x, p2.y],
    ]),
  };
}

function interpolate(a, b, frac) {
  // points A and B, frac between 0 and 1
  var nx = a.x + (b.x - a.x) * frac;
  var ny = a.y + (b.y - a.y) * frac;
  return {
    x: nx,
    y: ny,
    check: polygon([
      [a.x, a.y],
      [b.x, b.y],
      [a.x, b.y],
    ]),
  };
}

/*
[0, 0],
      [0, 21],
      [-18, 21],
      [-21, 17],
      [-21, 10],
      [-20, 0],
polygon([[0,0],[0,21],[-18,21],[-21,17],[-21,10],[-20,0]]);
*/
function WindowBase(endWidth = 60, middleWidth = 15, centerWidth = 12) {
  function endpiece(height) {
    return CSG.Polygon.createFromPoints([
      [0, 0, height],
      [0, 21, height],
      [-18, 21, height],
      [-21, 17, height],
      [-21, 10, height],
      [-20, 0, height],
    ]);
  }

  /*
  polygon([[0,0],[0,26],[-23,39.5],[-38,23.5],[-21,0]]);
*/
  function centerpiece(height) {
    return CSG.Polygon.createFromPoints([
      [0, 0, height],
      [0, 26, height],
      [-23, 39.5, height],
      [-38, 23.5, height],
      [-21, 0, height],
    ]);
  }

  var g = util.group("windowbase");
  var centerShape = polygon([
    [0, 0],
    [0, 26],
    [-23, 39.5],
    [-38, 23.5],
    [-21, 0],
  ]);
  var end = endpiece(endWidth).solidFromSlices({
    numslices: 2,
    callback: function(t) {
      return t ? endpiece(middleWidth) : endpiece(endWidth);
    },
  });
  //   g.add(end, "end");

  var thing = endpiece(middleWidth)
    .solidFromSlices({
      numslices: 2,
      callback: function(t) {
        return t ? centerpiece(centerWidth) : endpiece(middleWidth);
      },
    })
    .color("blue");
  //   g.add(thing, "thing");

  var center = centerpiece(centerWidth)
    .solidFromSlices({
      numslices: 2,
      callback: function(t) {
        return t ? centerpiece(0) : centerpiece(centerWidth);
      },
    })
    .color("green");

  //   g.add(center, "center");

  var hole = CSG.cylinder({
    start: [0, 0, 0],
    end: [0, 40, 0],
    radius: 1.5,
    resolution: 32,
  }).translate([0 - 5, 0, endWidth - 10]);

  var left = union(end, thing, center);
  g.add(
    left
      .union(left.mirroredZ())
      // .rotateX(90)
      .color("brown"),
    "base"
  );

  g.tri = util.triangle.solve(
    {
      x: -23,
      y: 39.5,
    },
    {
      x: -38,
      y: 23.5,
    }
  );
  g.c = interpolate(
    {
      x: -23,
      y: 39.5,
    },
    {
      x: -38,
      y: 23.5,
    },
    0.5
  );

  // util
  //   .unitAxis(25, 0.25)
  //   .rotateZ(windowbase.tri.b)
  //   .translate([windowbase.c.x, windowbase.c.y, 0]),
  g.parts.base.properties.driveConnector = new CSG.Connector(
    [g.c.x, g.c.y, 0],
    [g.c.y, g.c.x, 0],
    [g.c.y, g.c.x, 1]
  );

  // console.log("g.check", g);
  // g.add(util.poly2solid(g.c.check, g.c.check, 1), "center");
  return g;
}
